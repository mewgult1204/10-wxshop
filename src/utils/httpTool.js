import Vue from 'vue'
import { formUrl } from './toPage';

const httpTool = ({timeout,baseUrl,errorHandler,requestHandler}) => {
    const request = async (url,method = 'get',data={},header={}) => {
        const requestObject = {
            url:`${baseUrl}${url}`,
            method,
            data,
            header,
            timeout
        }
        const config = requestHandler && requestHandler(requestObject) || requestObject;
        if(typeof config === 'object' && config.url && config.method){
            try{
                const res = await uni.request(config);
                if(Array.isArray(res) && res.length && res[1]){
                    return res[1].data ? res[1].data : res[1];
                }
                return Promise.reject(res);
            }catch(error){
                errorHandler && errorHandler(error);
                return Promise.reject(error);
            }
        }
    }
    return {
        post(url,data,header={}){
            return request(url,'POST',data,header)
        },
        get(url,query,header={}){
           url = formUrl(url,query);
           return request(url,'GET',{},header)
        }
    }
}

const http = httpTool({
    timeout: 10000,
    baseUrl: 'https://bjwz.bwie.com/mall4j',
    requestHandler: (config) => {
        return {
            ...config,
            header: {
                'test': 'test'
            }
        }
    },
    errorHandler: (error) => {

    }
})

export default http;

Vue.prototype.$http = http;